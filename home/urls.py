from django.conf.urls import url
from . import views
from django.urls import path

urlpatterns = [
    path('', views.home, name='home'),
    path('login/', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    path('createStudent/', views.createStudent, name='createStudent'),
    path('createStudent/addStudent/', views.addStudent, name='addStudent'),
    path('viewStudent/<str:rollNo>/', views.viewStudent, name='viewStudent'),
    path('updateStudent/<str:rollNo>/', views.updateStudent, name='updateStudent'),
    path('updateStudentDetails/<str:rollNo>/', views.updateStudentDetails, name='updateStudentDetails'),
    path('deleteStudent/<str:rollNo>/', views.deleteStudent, name='deleteStudent'),
    path('createUser/', views.createUser, name='createUser'),
    path('loginUser/', views.loginUser, name='loginUser'),
    path('logout/', views.logout, name='logout'),
    path('changePassword/<str:userId>', views.changePassword, name='changePassword'),
    path('search/', views.search, name='search'),
    path('searchWebsite/', views.searchWebsite, name='searchWebsite'),
]
from django.contrib import admin
from .models import LoginDetails, StudentInfo, StudentAcademics

admin.site.register(LoginDetails)
admin.site.register(StudentInfo)
admin.site.register(StudentAcademics)
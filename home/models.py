from django.db import models
from django.db.models.fields import CharField

class LoginDetails(models.Model):
    user_id = models.CharField(max_length=20)
    password = models.CharField(max_length=20)

class StudentInfo(models.Model):
    roll_no = models.CharField(max_length=10, primary_key=True)
    name = models.CharField(max_length=50)
    Class = models.CharField(max_length=10)
    school = models.CharField(max_length=100)
    mobile = models.CharField(max_length=10)
    address = models.CharField(max_length=200)

class StudentAcademics(models.Model):
    roll_no = models.ForeignKey(StudentInfo, on_delete=models.CASCADE)
    maths = models.IntegerField()
    physics = models.IntegerField()
    chemistry = models.IntegerField()
    biology = models.IntegerField()
    english = models.IntegerField()


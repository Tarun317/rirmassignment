from django.shortcuts import render, redirect
from home.models import LoginDetails, StudentInfo, StudentAcademics
import requests
from bs4 import BeautifulSoup

def home(request):
    students = StudentInfo.objects.all()
    context = {
        "students": students
    }
    return render(request, 'home/home.html', context)

def viewStudent(request, rollNo):
    studentObject = StudentInfo.objects.get(roll_no=rollNo)
    studentAcademicsObject = StudentAcademics.objects.get(roll_no=studentObject)
    context = {
        "student_academics": studentAcademicsObject,
        "student_object": studentObject
    }
    return render(request, 'home/viewStudent.html', context)

def updateStudent(request, rollNo):
    studentObject = StudentInfo.objects.get(roll_no=rollNo)
    studentAcademicsObject = StudentAcademics.objects.get(roll_no=studentObject)
    context = {
        "student_academics": studentAcademicsObject,
        "student_object": studentObject
    }
    return render(request, 'home/updateStudent.html', context)

def updateStudentDetails(request, rollNo):
    studentObject = StudentInfo.objects.get(roll_no=rollNo)
    studentAcademicsObject = StudentAcademics.objects.get(roll_no=studentObject)

    studentObject.name = request.POST['name']
    studentObject.Class = request.POST['class']
    studentObject.school = request.POST['school']
    studentObject.mobile = request.POST['mobile']
    studentObject.address = request.POST['address']
    studentAcademicsObject.maths = int(request.POST['maths']) if request.POST['maths'] != '' else 0
    studentAcademicsObject.physics = int(request.POST['physics']) if request.POST['physics'] != '' else 0
    studentAcademicsObject.chemistry = int(request.POST['chemistry']) if request.POST['chemistry'] != '' else 0
    studentAcademicsObject.biology = int(request.POST['biology']) if request.POST['biology'] != '' else 0
    studentAcademicsObject.english = int(request.POST['english']) if request.POST['english'] != '' else 0

    studentObject.save()
    studentAcademicsObject.save()

    return redirect('/home/')
    
def deleteStudent(request, rollNo):
    studentObject = StudentInfo.objects.get(roll_no=rollNo)
    studentObject.delete()
    return redirect('/home/')

def createStudent(request):
    if 'userId' in request.session:
        errorMessage = ""
        response = ""
    else:
        return redirect('/home/')
    context = {
        "error_message": errorMessage,
        "response": response
    }
    return render(request, 'home/createStudent.html', context)

def addStudent(request):
    errorMessage = ""
    response = ""
    rollNo = request.POST['roll_no']
    
    if rollNo == '':
        errorMessage = "Please enter valid Roll Number"
    
    elif len(StudentInfo.objects.filter(roll_no=request.POST['roll_no'])) > 0:
        errorMessage = "Student already exists"
    else:
        studentObject = StudentInfo()
        studentObject.roll_no = int(rollNo)
        studentObject.name = request.POST['name']
        studentObject.Class = request.POST['class']
        studentObject.school = request.POST['school']
        studentObject.mobile = request.POST['mobile']
        studentObject.address = request.POST['address']

        studentAcademicsObject = StudentAcademics()
        studentAcademicsObject.roll_no = studentObject
        studentAcademicsObject.maths = int(request.POST['maths']) if request.POST['maths'] != '' else 0
        studentAcademicsObject.physics = int(request.POST['physics']) if request.POST['physics'] != '' else 0
        studentAcademicsObject.chemistry = int(request.POST['chemistry']) if request.POST['chemistry'] != '' else 0
        studentAcademicsObject.biology = int(request.POST['biology']) if request.POST['biology'] != '' else 0
        studentAcademicsObject.english = int(request.POST['english']) if request.POST['english'] != '' else 0

        studentObject.save()
        studentAcademicsObject.save()

        response = "Student Created"

    context = {
        "error_message": errorMessage,
        "response": response
    }
    return render(request, 'home/createStudent.html', context)

def login(request):
    if 'userId' in request.session:
        if request.session['userId']:
            return redirect('/home/')
    errorMessage = ""
    context = {
        "error_message": errorMessage,
    }
    return render(request, 'home/login.html', context)

def signup(request):
    if 'userId' in request.session:
        if request.session['userId']:
            return redirect('/home/')
    errorMessage = ""
    response = ""
    context = {
        "error_message": errorMessage,
        "response": response
    }
    return render(request, 'home/signup.html', context)

def createUser(request):
    errorMessage = ""
    response = ""
    userId = request.POST['user_id']
    password = request.POST['password']

    if userId == '' or password == '':
        errorMessage = "Please enter valid details"
    else:
        try:
            verifyAccountId = LoginDetails.objects.get(user_id=userId)
        except LoginDetails.DoesNotExist:
            verifyAccountId = None

        if verifyAccountId == None:
            createUserObject = LoginDetails()
            createUserObject.user_id = userId
            createUserObject.password = password
            createUserObject.save()
            response = "User Created"
        else:
            errorMessage = "User Id already exists"

    context = {
        "error_message": errorMessage,
        "response": response
    }
    return render(request, 'home/signup.html', context)

def loginUser(request):
    errorMessage = ""
    userId = request.POST['user_id']
    password = request.POST['password']

    if userId == '' or password == '':
        errorMessage = "Please enter valid details"
    else:
        try:
            verifyAccountId = LoginDetails.objects.get(user_id=userId)
        except LoginDetails.DoesNotExist:
            verifyAccountId = None
        if verifyAccountId == None:
            errorMessage = "User doesn't exists"
        else:
            if verifyAccountId.password == password:
                request.session['userId'] = userId
                return redirect('/home/')
            else:
                errorMessage = "Incorrect Password"
    context = {
        "error_message": errorMessage
    }
    return render(request, 'home/login.html', context)

def logout(request):
    del request.session['userId']
    return redirect('/home/')

def changePassword(request, userId):
    if 'userId' in request.session:
        userObj = LoginDetails.objects.get(user_id=userId)
        errorMessage = ""
        response = ""
        oldPassword = request.POST.get('old_password',False)
        newPassword = request.POST.get('new_password',False)
        if oldPassword == False and newPassword == False:
            errorMessage = ""
        elif oldPassword == '' or newPassword == '':
            errorMessage = "Please enter old and new password"
        elif userObj.password != oldPassword:
            errorMessage = "Incorrect Old Password"
        elif oldPassword == newPassword:
            errorMessage = "Old and New password can't be same"
        else:
            userObj.password = newPassword
            userObj.save()
            response = "Password changed"
    else:
        return redirect('/home/')

    context = {
        "error_message": errorMessage,
        "response": response
    }
    return render(request, 'home/changePassword.html', context)

def search(request):
    search = request.POST.get('search',False)
    students = StudentInfo.objects.filter(name__contains=search)
    context = {
        "students": students
    }
    return render(request, 'home/home.html', context)

def searchWebsite(request):
    urls = []
    search = request.POST.get('search',False)
    if search != False:
        req = requests.get(search)
        soup = BeautifulSoup(req.text, 'html.parser')

        urls = []
        for link in soup.find_all('a'):
            url = link.get('href')
            if url != None:
                if (url.find('http') != -1):
                    urls.append(url)
        
    context = {"urls":urls}
    return render(request, 'home/searchWebsite.html', context)
        
